package com.papps.flazo.appblue.common

import android.content.Context
import android.content.SharedPreferences
import com.papps.flazo.appblue.R

class Preferences private constructor(private val preferences: SharedPreferences){

    var tokenID: String
        get() = preferences.getString(TOKEN_ID_KEY, "") ?: ""
        set(value) = preferences.edit().putString(TOKEN_ID_KEY, value).apply()

    companion object {
        private var preferencesInstance: Preferences? = null

        private const val TOKEN_ID_KEY = "token_id_key"

        fun instance(context: Context): Preferences {
            if (preferencesInstance == null)
                preferencesInstance = Preferences(context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE))
            return preferencesInstance!!
        }
    }
}