package com.papps.flazo.appblue.common.util

import android.content.Context
import android.net.ConnectivityManager

object CommonUtil {

    fun isThereInternetConnection(context: Context): Boolean {
        val isConnected: Boolean

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        isConnected = networkInfo != null && networkInfo.isConnected

        return isConnected
    }
}