package com.papps.flazo.appblue.common.util

object ServiceConstants {

    const val TEST_PATH: String = "blue"
    const val LOGIN_PATH: String = "api/authenticate"

}