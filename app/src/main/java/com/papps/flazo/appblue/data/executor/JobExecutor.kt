package com.papps.flazo.appblue.data.executor

import java.util.concurrent.*

object JobExecutor : Executor {

    private const val THREAD_NAME = "android_ibk_"
    private const val INITIAL_POOL_SIZE = 3
    private const val MAXIMUM_POOL_SIZE = 3
    private const val KEEP_ALIVE_TIME = 3L
    private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS

    private val threadPoolExecutor = ThreadPoolExecutor(
            INITIAL_POOL_SIZE,
            MAXIMUM_POOL_SIZE,
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            LinkedBlockingDeque(),
            JobThreadFactory())

    override fun execute(command: Runnable) {
        threadPoolExecutor.execute(command)
    }

    private class JobThreadFactory : ThreadFactory {
        private var counter = 0

        override fun newThread(r: Runnable): Thread {
            return Thread(r, THREAD_NAME + counter++)
        }
    }
}
