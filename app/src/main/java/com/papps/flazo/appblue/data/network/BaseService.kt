package com.papps.flazo.appblue.data.network

import android.content.Context
import com.papps.flazo.appblue.common.util.CommonUtil
import io.reactivex.Flowable
import io.reactivex.FlowableOnSubscribe
import java.net.ConnectException

open class BaseService(private val context: Context) {

    fun <T> consume(service: Flowable<T>): FlowableOnSubscribe<T> = FlowableOnSubscribe { emitter ->
        if (CommonUtil.isThereInternetConnection(context))
            service.subscribe(emitter::onNext, emitter::onError, emitter::onComplete)
        else emitter.onError(ConnectException())
    }
}