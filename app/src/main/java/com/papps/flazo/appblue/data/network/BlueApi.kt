package com.papps.flazo.appblue.data.network

import com.papps.flazo.appblue.BuildConfig
import com.papps.flazo.appblue.ui.Session
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object BlueApi {

    private const val TIMEOUT: Long = 20
    private var retrofit: Retrofit? = createRetrofit()


    private fun createRetrofit(minimumTime: Long = TIMEOUT): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE



        val httpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(minimumTime, TimeUnit.SECONDS)
            .writeTimeout(minimumTime, TimeUnit.SECONDS)
            .readTimeout(minimumTime, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://bluepointsapp-env.m3jddumvfq.us-east-2.elasticbeanstalk.com/")
            .client(httpClient)
            .build()
    }


    fun <S> createService(service: Class<S>): S {
        if (retrofit == null) retrofit = createRetrofit()
        return retrofit!!.create(service)
    }

}