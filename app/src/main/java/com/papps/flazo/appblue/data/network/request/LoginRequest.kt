package com.papps.flazo.appblue.data.network.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginRequest() : Serializable {

    @SerializedName("email")
    var email: String? = null
    @SerializedName("password")
    var password: String? = null

    constructor(email: String, password: String) : this() {
        this.email = email
        this.password = password
    }
}