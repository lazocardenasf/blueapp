package com.papps.flazo.appblue.data.network.response

import com.google.gson.annotations.SerializedName

class LoginResponse(
    @SerializedName("accessToken")
    var accessToken: String? = null,
    @SerializedName("tokenType")
    var tokenType: String? = null
)