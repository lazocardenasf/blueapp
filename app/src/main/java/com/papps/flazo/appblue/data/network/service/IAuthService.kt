package com.papps.flazo.appblue.data.network.service

import com.papps.flazo.appblue.common.util.ServiceConstants
import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.network.response.LoginResponse
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface IAuthService {

    @GET(ServiceConstants.TEST_PATH)
    fun testApp() : Flowable<String>

    @POST(ServiceConstants.LOGIN_PATH)
    fun login(@Body request: LoginRequest): Flowable<LoginResponse>
}