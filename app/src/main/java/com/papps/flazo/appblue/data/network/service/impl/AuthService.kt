package com.papps.flazo.appblue.data.network.service.impl

import android.content.Context
import com.papps.flazo.appblue.data.network.BaseService
import com.papps.flazo.appblue.data.network.BlueApi
import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.network.response.LoginResponse
import com.papps.flazo.appblue.data.network.service.IAuthService
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

class AuthService(context: Context) : BaseService(context), IAuthService {

    override fun login(request: LoginRequest): Flowable<LoginResponse> = Flowable.create(
        consume(BlueApi.createService(IAuthService::class.java).login(request)),
        BackpressureStrategy.BUFFER
    )

    override fun testApp(): Flowable<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}