package com.papps.flazo.appblue.data.repository.auth

import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.network.response.LoginResponse
import com.papps.flazo.appblue.data.network.service.IAuthService
import io.reactivex.Flowable
import retrofit2.Response


class AuthCloudDataStore internal constructor(private val authService: IAuthService) :
    AuthDataStore {

    override fun login(request: LoginRequest): Flowable<LoginResponse> = authService.login(request)
}