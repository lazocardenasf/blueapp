package com.papps.flazo.appblue.data.repository.auth

import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.network.response.LoginResponse
import io.reactivex.Flowable
import retrofit2.Response


interface AuthDataStore {

    fun login(request: LoginRequest): Flowable<LoginResponse>


}