package com.papps.flazo.appblue.data.repository.auth

import android.content.Context
import com.papps.flazo.appblue.data.network.service.impl.AuthService

object AuthDataStoreFactory {

    fun createCloud(context: Context): AuthCloudDataStore = AuthCloudDataStore(AuthService(context))

}
