package com.papps.flazo.appblue.data.repository.auth

import android.content.Context
import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.network.response.LoginResponse
import io.reactivex.Flowable
import retrofit2.Response

class AuthRepository(private val context: Context) {

    fun login(request: LoginRequest): Flowable<LoginResponse> = AuthDataStoreFactory.createCloud(context).login(request)


}