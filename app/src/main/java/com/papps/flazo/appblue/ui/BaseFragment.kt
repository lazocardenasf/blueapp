package com.papps.flazo.appblue.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    protected abstract val view: Int
    private var activity: BaseActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(view, container, false)


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as BaseActivity
    }


    override fun onDetach() {
        super.onDetach()
        activity = null
    }
}