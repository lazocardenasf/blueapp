package com.papps.flazo.appblue.ui

import android.os.Parcel
import android.os.Parcelable
import com.papps.flazo.appblue.common.Preferences

class Session private constructor() :Parcelable{

    var tokenID: String? = null

    constructor(parcel: Parcel) : this() {
        tokenID = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tokenID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Session> {
        override fun createFromParcel(parcel: Parcel): Session {
            return Session(parcel)
        }

        override fun newArray(size: Int): Array<Session?> {
            return arrayOfNulls(size)
        }
        private var instance: Session? = null


        fun getInstance(): Session {
            if (instance == null) {
                instance = Session()
            }
            return instance!!
        }

        fun clearInstance() {
            instance = null
        }

        fun reInstantiate(pref: Preferences): Session {
            if (instance == null) {
                instance = Session().apply {
                    tokenID = pref.tokenID
                    //user = pref.user
                }
            }
            return instance!!
        }
    }

}