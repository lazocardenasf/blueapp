package com.papps.flazo.appblue.ui.custom

import android.view.View
private var lastTimeClicked: Long = 0


fun View.onClickListener(listener: (View) -> Unit) {
    this.setOnClickListener {
        if (System.currentTimeMillis() - lastTimeClicked >= 300) {
            listener(it)
            lastTimeClicked = System.currentTimeMillis()
        }
    }
}
