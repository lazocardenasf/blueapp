package com.papps.flazo.appblue.ui.login.view

import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseActivity

class LoginActivity : BaseActivity() {
    override val view: Int
        get() = R.layout.activity_login

}