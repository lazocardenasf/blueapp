package com.papps.flazo.appblue.ui.login.view

import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseFragment

class LoginFragment : BaseFragment() {
    override val view: Int
        get() = R.layout.fragment_login
}