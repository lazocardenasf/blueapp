package com.papps.flazo.appblue.ui.model

data class LoginModel(
    var accessToken: String? = null,
    var tokenType: String? = null
)