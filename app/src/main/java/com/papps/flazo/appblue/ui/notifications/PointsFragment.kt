package com.papps.flazo.appblue.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.papps.flazo.appblue.R

class PointsFragment : Fragment() {

    private lateinit var pointsViewModel: PointsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pointsViewModel =
            ViewModelProviders.of(this).get(PointsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)
        val textView: TextView = root.findViewById(R.id.text_notifications)
        pointsViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}