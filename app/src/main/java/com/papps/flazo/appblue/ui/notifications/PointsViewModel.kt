package com.papps.flazo.appblue.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PointsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is bluePoints Fragment"
    }
    val text: LiveData<String> = _text
}