package com.papps.flazo.appblue.ui.splash.view

import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseActivity

class SplashActivity : BaseActivity() {

    override val view: Int
        get() = R.layout.actvity_splash
}