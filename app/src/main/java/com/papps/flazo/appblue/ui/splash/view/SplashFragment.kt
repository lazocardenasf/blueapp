package com.papps.flazo.appblue.ui.splash.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseFragment
import com.papps.flazo.appblue.ui.main.MainActivity
import com.papps.flazo.appblue.ui.model.LoginModel
import com.papps.flazo.appblue.viewmodel.interactor.AuthInteractorFactory
import com.papps.flazo.appblue.viewmodel.interactor.LoginInteractor
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class SplashFragment : BaseFragment() {

    private lateinit var loginInteractor: LoginInteractor


    private lateinit var disposable: Disposable
    override val view: Int
        get() = R.layout.fragment_splash

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        startDelay()
        //loginInteractor.login()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val authFactory = AuthInteractorFactory(activity!!)
        with(ViewModelProviders.of(this, authFactory)[LoginInteractor::class.java]){
            loginInteractor = this
            loginData.observe(this@SplashFragment, Observer { logIn(it) })
        }
    }


    private fun logIn(login: LoginModel) {
        Log.d("as", "asa")
    }

    private fun startDelay() {
        disposable = Observable.interval(5, TimeUnit.SECONDS)
            .doOnNext { goToWelcome() }
            .subscribe()
    }

    private fun goToWelcome() {
        startActivity(Intent(activity, MainActivity::class.java))
        activity?.finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!disposable.isDisposed) disposable.dispose()
    }
}