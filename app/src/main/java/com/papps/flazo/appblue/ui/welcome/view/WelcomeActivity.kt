package com.papps.flazo.appblue.ui.welcome.view

import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseActivity

class WelcomeActivity : BaseActivity() {

    override val view: Int
        get() = R.layout.activity_welcome
}