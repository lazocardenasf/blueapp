package com.papps.flazo.appblue.ui.welcome.view

import com.papps.flazo.appblue.R
import com.papps.flazo.appblue.ui.BaseFragment

class WelcomeFragment : BaseFragment() {
    override val view: Int
        get() = R.layout.fragment_welcome
}