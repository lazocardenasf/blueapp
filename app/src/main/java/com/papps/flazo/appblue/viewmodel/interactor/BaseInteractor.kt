package com.papps.flazo.appblue.viewmodel.interactor

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.papps.flazo.appblue.ui.model.ErrorModel
import com.papps.flazo.appblue.viewmodel.mapper.ErrorMapper
import io.reactivex.subscribers.DisposableSubscriber

abstract class BaseInteractor : ViewModel() {

    val loadingData: MutableLiveData<Boolean> = MutableLiveData()
    val errorData: MutableLiveData<ErrorModel> = MutableLiveData()

    fun showLoading() = loadingData.postValue(true)
    fun hideLoading() = loadingData.postValue(false)
    fun showError(parser: () -> ErrorModel) = errorData.postValue(parser())

    abstract fun init()

    abstract fun dispose()

    abstract inner class BaseSubscriber<T> : DisposableSubscriber<T>() {
        override fun onComplete() {
            hideLoading()
        }

        override fun onNext(value: T) {
            hideLoading()
        }

        override fun onError(error: Throwable) {
            Log.d(TAG, error.message, error)
            showError { ErrorMapper.transform(error) }
        }
    }

    companion object {
        val TAG: String = BaseInteractor::class.java.canonicalName.orEmpty()
    }
}