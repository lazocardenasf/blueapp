package com.papps.flazo.appblue.viewmodel.interactor

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.papps.flazo.appblue.data.repository.auth.AuthRepository
import com.papps.flazo.appblue.viewmodel.usecase.LoginUseCase


@Suppress("UNCHECKED_CAST")
class AuthInteractorFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val authRepository = AuthRepository(context)

        val loginUseCase = LoginUseCase(authRepository)


        return LoginInteractor(loginUseCase) as T
    }
}
