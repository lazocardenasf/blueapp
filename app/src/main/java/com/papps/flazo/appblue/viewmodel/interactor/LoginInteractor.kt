package com.papps.flazo.appblue.viewmodel.interactor

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.ui.model.LoginModel
import com.papps.flazo.appblue.viewmodel.usecase.LoginUseCase
import retrofit2.HttpException

class LoginInteractor(private val loginUseCase: LoginUseCase) : BaseInteractor() {

    val loginData: MutableLiveData<LoginModel> = MutableLiveData()


    override fun init() {
        loginUseCase.init()
    }

    override fun dispose() {
        loginUseCase.dispose()
    }


    fun login() {
        val loginRequest = LoginRequest("lgomero94@gmail.com", "admin123")
        loginUseCase.login(request = loginRequest, observer = LoginSubscriber())
    }


    private inner class LoginSubscriber : BaseSubscriber<LoginModel>() {
        override fun onNext(value: LoginModel) {
            loginData.postValue(value)
        }

        override fun onError(error: Throwable) {

        }
    }
}