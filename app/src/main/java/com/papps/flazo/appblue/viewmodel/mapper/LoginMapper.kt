package com.papps.flazo.appblue.viewmodel.mapper

import com.papps.flazo.appblue.data.network.response.LoginResponse
import com.papps.flazo.appblue.ui.model.LoginModel


object LoginMapper {

    fun transform(loginResponse: LoginResponse): LoginModel = LoginModel(loginResponse.accessToken,loginResponse.tokenType)
}