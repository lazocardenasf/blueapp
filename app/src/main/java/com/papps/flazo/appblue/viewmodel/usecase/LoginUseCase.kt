package com.papps.flazo.appblue.viewmodel.usecase

import com.papps.flazo.appblue.data.network.request.LoginRequest
import com.papps.flazo.appblue.data.repository.auth.AuthRepository
import com.papps.flazo.appblue.ui.model.LoginModel
import com.papps.flazo.appblue.viewmodel.mapper.LoginMapper
import io.reactivex.subscribers.DisposableSubscriber

class LoginUseCase(private val authRepository: AuthRepository) : UseCase() {

    fun login(request: LoginRequest, observer: DisposableSubscriber<LoginModel>) {
        execute(authRepository.login(request).map { LoginMapper.transform(it) }, observer)
    }
}
