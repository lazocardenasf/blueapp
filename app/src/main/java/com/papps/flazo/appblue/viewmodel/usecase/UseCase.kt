package com.papps.flazo.appblue.viewmodel.usecase

import com.papps.flazo.appblue.data.executor.JobExecutor
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber

open class UseCase {

    private var disposables = CompositeDisposable()

    protected fun <T> execute(flowable: Flowable<T>, observer: DisposableSubscriber<T>) {
        val disposable = flowable
                .subscribeOn(Schedulers.from(JobExecutor))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer)

        disposables.add(disposable)
    }

    fun init() {
        if (disposables.isDisposed)
            disposables = CompositeDisposable()
    }

    fun dispose() {
        disposables.dispose()
        disposables.clear()
    }
}
